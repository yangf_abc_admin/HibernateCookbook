--正常表
create table t_user(
	id varchar(36) primary key auto_increment,
	username varchar(50),
	password varchar(50),
	birth datetime,
	email varchar(100),
	mobile varchar(11),
	regist_date datetime,
	vip	int(1),
	status int(1)
)engine=innodb,charset=utf8;
insert into t_user values(uuid(),'yangfan1','123456',sysdate(),'yangdd@qq.com','12345678756',sysdate(),1,1);
insert into t_user values(uuid(),'yangfan2','123456',sysdate(),'yangdd@qq.com','12345678756',sysdate(),1,1);
insert into t_user values(uuid(),'yangfan3','123456',sysdate(),'yangdd@qq.com','12345678756',sysdate(),1,1);
insert into t_user values(uuid(),'yangfan4','123456',sysdate(),'yangdd@qq.com','12345678756',sysdate(),1,1);
insert into t_user values(uuid(),'yangfan5','123456',sysdate(),'yangdd@qq.com','12345678756',sysdate(),1,1);
commit;


--MySQL主键自增表
create table t_user(
	id bigint(10) primary key auto_increment,
	username varchar(50),
	password varchar(50),
	birth datetime,
	email varchar(100),
	mobile varchar(11),
	regist_date datetime,
	vip	int(1),
	status int(1)
)engine=innodb,charset=utf8;
insert into t_user(username,password,birth,email,mobile,regist_date,vip,status) values('yangfan1','123456',sysdate(),'yangdd@qq.com','12345678756',sysdate(),1,1);
insert into t_user(username,password,birth,email,mobile,regist_date,vip,status) values('yangfan2','123456',sysdate(),'yangdd@qq.com','12345678756',sysdate(),1,1);
insert into t_user(username,password,birth,email,mobile,regist_date,vip,status) values('yangfan3','123456',sysdate(),'yangdd@qq.com','12345678756',sysdate(),1,1);
insert into t_user(username,password,birth,email,mobile,regist_date,vip,status) values('yangfan4','123456',sysdate(),'yangdd@qq.com','12345678756',sysdate(),1,1);
insert into t_user(username,password,birth,email,mobile,regist_date,vip,status) values('yangfan5','123456',sysdate(),'yangdd@qq.com','12345678756',sysdate(),1,1);
commit;

