create table t_generator(
	id int UNSIGNED primary key auto_increment,
	pk_name varchar(20),
	pk_value bigint unsigned
)engine=innodb,default charset=utf8;

insert into t_generator(pk_name,pk_value) values("product_id",10);
commit;