package com.baizhi.demo1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static final SessionFactory factory;
	static {
		// 1. 初始化peizhiwenjia9n
		Configuration configure = new Configuration().configure();
		// 2. 初始化服务配置。
		StandardServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(configure.getProperties())
				.build();
		// 3. 创建session工厂
		factory = configure.buildSessionFactory(registry);
	}

	public static Session getCurrentSession() {

		// 4. 获得当前会话session
		Session session = factory.getCurrentSession();
		return session;
	}
}
