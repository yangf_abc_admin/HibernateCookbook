package com.baizhi.demo1;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

public class HibernateTest {

	@Test
	public void test1() {
		// 1. 初始化peizhiwenjia9n
		Configuration configure = new Configuration().configure();
		// 2. 初始化服务配置。
		StandardServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(configure.getProperties())
				.build();
		// 3. 创建session工厂
		SessionFactory factory = configure.buildSessionFactory(registry);
		// 4. 获得当前会话session
		Session session = factory.getCurrentSession();
		System.out.println(session);
		// 5. 开启事务
		Transaction tx = session.beginTransaction();
		User u = (User) session.get(User.class, 60L);
		System.out.println(u.getClass().getSimpleName());
		System.out.println(u.getId());
		System.out.println(u.getClass().getSimpleName());
		// 提交事务
		tx.commit();//如果session是getCurrentSession得到的，该代码会提交事务并且关闭session
		// 工厂关闭
		factory.close();
	}

	@Test
	public void test2() {
		// 1. 初始化peizhiwenjia9n
		Configuration configure = new Configuration().configure();
		// 2. 初始化服务配置。
		StandardServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(configure.getProperties())
				.build();
		// 3. 创建session工厂
		SessionFactory factory = configure.buildSessionFactory(registry);
		// 4. 获得当前会话session
		Session session = factory.getCurrentSession();

		System.out.println(session);
		// 5. 开启事务
		Transaction tx = session.beginTransaction();
		User u = new User("哈哈1", "123123", new Date(), "hehe", "12345678", new Date(), 0, 1);
		session.save(u);
		// 提交事务
		tx.commit();//关闭事务，同时会关闭session
		// 事务结束
		//session.close();
		// 工厂关闭
		factory.close();
	}
	@Test
	public void test3() {
		Session session1 = HibernateUtil.getCurrentSession();
		Session session2 = HibernateUtil.getCurrentSession();
		
		session2.close();
		
		Session session3 = HibernateUtil.getCurrentSession();
		Session session4 = HibernateUtil.getCurrentSession();
		System.out.println(session1.hashCode());
		System.out.println(session2.hashCode());
		
		System.out.println(session3.hashCode());
		System.out.println(session4.hashCode());
	}
}
