package com.baizhi.demo1;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.FetchProfile.FetchOverride;
@Entity
@Table(name="t_user")
public class User implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	@Basic(fetch=FetchType.EAGER)
	private String username;
	@Basic(fetch=FetchType.EAGER)
	private String password;
	@Column
	@Basic(fetch=FetchType.LAZY)
	@Temporal(TemporalType.DATE)
	private Date birth;
	@Basic(fetch=FetchType.LAZY)
	private String email;
	@Basic(fetch=FetchType.LAZY)
	private String mobile;
	@Column(name="regist_date")
	@Basic(fetch=FetchType.LAZY)
	@Temporal(TemporalType.DATE)
	private Date registDate;
	@Basic(fetch=FetchType.LAZY)
	private Integer vip;
	@Basic(fetch=FetchType.LAZY)
	private Integer status;

	public User() {}

	
	

	public User(String username, String password, Date birth, String email, String mobile, Date registDate,
			Integer vip, Integer status) {
		super();
		this.username = username;
		this.password = password;
		this.birth = birth;
		this.email = email;
		this.mobile = mobile;
		this.registDate = registDate;
		this.vip = vip;
		this.status = status;
	}
	@Basic(fetch=FetchType.EAGER)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Basic(fetch=FetchType.EAGER)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	@Basic(fetch=FetchType.EAGER)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	@Basic(fetch=FetchType.LAZY)
	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}
	@Basic(fetch=FetchType.LAZY)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	@Basic(fetch=FetchType.LAZY)
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	
	@Basic(fetch=FetchType.LAZY)
	public Integer getVip() {
		return vip;
	}

	public void setVip(Integer vip) {
		this.vip = vip;
	}
	@Basic(fetch=FetchType.LAZY)
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Basic(fetch=FetchType.LAZY)
	public Date getRegistDate() {
		return registDate;
	}

	public void setRegistDate(Date registDate) {
		this.registDate = registDate;
	}




	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", birth=" + birth + ", email="
				+ email + ", mobile=" + mobile + ", registDate=" + registDate + ", vip=" + vip + ", status=" + status
				+ "]";
	}
}
