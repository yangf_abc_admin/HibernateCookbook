package com.baizhi.demo1;

import java.util.List;

public interface UserDAO {
	void insert(User user);
	void update(User user);
	void delete(User id);
	User selectById(Long id);
	List<User> selectList();
	void batchDelete(Long[] ids);
	
	void batchInsert(List<User> users);
}
