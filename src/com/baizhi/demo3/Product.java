package com.baizhi.demon;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="t_product")
public class Product {
	
	@Id
	@TableGenerator(
			name="tb_generator",
			table="t_generator",
			initialValue=5,
			pkColumnName="pk_name",
			valueColumnName="pk_value",
			pkColumnValue="product_id",
			allocationSize=10
			)
	@GeneratedValue(generator="tb_generator",strategy=GenerationType.TABLE)
	private Long id;
	@Column(name="product_name")
	private String productName;
	@Column(name="price")
	private Double price;
	public Product() {}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}


	public Double getPrice() {
		return price;
	}


	public void setPrice(Double price) {
		this.price = price;
	}


	public Product(String productName, Double price) {
		super();
		this.productName = productName;
		this.price = price;
	}
	
	
	
}
